package com.nvtrung.dp.entity;

public class HìnhTròn extends Hình {
	private double bánKính;
	
	public double getBánKính() {
		return bánKính;
	}

	public void setBánKính(double bánKính) {
		this.bánKính = bánKính;
	}

	@Override
	public String getInfo() {
		return String.format("Hình tròn, bán kính %5.2f", this.getBánKính());
	}

	public HìnhTròn(double bánKính) {
		this.bánKính = bánKính;
	}

	public double getChuVi() {
		return this.getBánKính() * 2 * Math.PI;
	}
}
