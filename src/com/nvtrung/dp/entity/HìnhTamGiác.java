package com.nvtrung.dp.entity;

public class HìnhTamGiác extends Hình {
	private double a, b, c;

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}

	@Override
	public String getInfo() {
		return String.format("Hình tam giác %5.2f, %5.2f, %5.2f", this.getA(), this.getB(), this.getC());
	}

	public HìnhTamGiác(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public double getChuVi() {
		return (this.getA() + this.getB() + this.getC());
	}

}
