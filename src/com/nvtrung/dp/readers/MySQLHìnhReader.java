package com.nvtrung.dp.readers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.nvtrung.dp.entity.Hình;
import com.nvtrung.dp.entity.HìnhChữNhật;
import com.nvtrung.dp.entity.HìnhTamGiác;
import com.nvtrung.dp.entity.HìnhTròn;

public class MySQLHìnhReader implements IHìnhReader {

	@Override
	public List<Hình> nạpDanhSáchHình() {
		List<Hình> lst = new ArrayList<>();

		// Kết nối đến csdl MySQL
		Connection cnn;
		try {
			cnn = DriverManager.getConnection("jdbc:mysql://localhost/hinhdb", "root", "123");
			Statement stmt = cnn.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT loai, f1, f2, f3 FROM hinh");

			Hình x = null;
			while (rs.next()) {
				int loai = rs.getInt(1);
				if (loai == 1) {
					// tam giác
					double f1 = rs.getDouble(2);
					double f2 = rs.getDouble(3);
					double f3 = rs.getDouble(4);
					x = new HìnhTamGiác(f1, f2, f3);
				} else if (loai == 2) {
					// tròn
					double r = rs.getDouble(2);
					x = new HìnhTròn(r);
				} else {
					// hình chữ nhật
					double f1 = rs.getDouble(2);
					double f2 = rs.getDouble(3);
					x = new HìnhChữNhật(f1, f2);
				}
				lst.add(x);
			}

			rs.close();
			stmt.close();
			cnn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lst;
	}
}
