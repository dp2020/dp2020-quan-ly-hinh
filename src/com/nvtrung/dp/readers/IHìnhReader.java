package com.nvtrung.dp.readers;
import java.util.List;

import com.nvtrung.dp.entity.Hình;

public interface IHìnhReader {
	
	/**
	 * nạp danh sách các đối tượng Hình và trả về danh sách này
	 * @return
	 */
	public List<Hình> nạpDanhSáchHình();
}
