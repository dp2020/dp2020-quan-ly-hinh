package com.nvtrung.dp.readers;

import java.util.ArrayList;
import java.util.List;

import com.nvtrung.dp.entity.Hình;
import com.nvtrung.dp.entity.HìnhChữNhật;
import com.nvtrung.dp.entity.HìnhTròn;

public class FakeReader implements IHìnhReader {

	@Override
	public List<Hình> nạpDanhSáchHình() {
		List<Hình> lst = new ArrayList<>();
		
		lst.add(new HìnhTròn(1.2));
		lst.add(new HìnhChữNhật(1, 2));
		
		return lst;
	}

}
